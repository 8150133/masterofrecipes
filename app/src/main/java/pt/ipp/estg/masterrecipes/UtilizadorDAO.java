package pt.ipp.estg.masterrecipes;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import pt.ipp.estg.masterrecipes.Utilizador;

@Dao
public interface UtilizadorDAO {
    @Query("SELECT * FROM Utilizador where email= :mail and password= :password")
    Utilizador getUser(String mail, String password);

    @Insert
    void insert(Utilizador utilizador);

    @Update
    void update(Utilizador utilizador);

    @Delete
    void delete(Utilizador utilizador);
}
