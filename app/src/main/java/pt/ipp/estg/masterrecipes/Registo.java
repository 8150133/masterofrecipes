package pt.ipp.estg.masterrecipes;

import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Registo extends AppCompatActivity {

    private EditText name;
    private EditText apelido;
    private EditText email;
    private EditText password;

    private Button cancelar;
    private Button registar;

    private UtilizadorDAO utilizadorDAO;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registo);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("A registar");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setProgress(0);


        name = findViewById(R.id.nameinput);
        apelido = findViewById(R.id.lastnameinput);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        cancelar = findViewById(R.id.btCancel);
        registar = findViewById(R.id.btRegister);

        utilizadorDAO = Room.databaseBuilder(this, UtilizadorDB.class, "mi-database.db")
                .allowMainThreadQueries()
                .build()
                .getUserDao();

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Registo.this, Login.class));
                finish();
            }
        });

        registar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isEmpty()) {

                    progressDialog.show();

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utilizador utilizador = new Utilizador(name.getText().toString(), apelido.getText().toString(),
                                    email.getText().toString(), password.getText().toString());
                            utilizadorDAO.insert(utilizador);
                            progressDialog.dismiss();
                            startActivity(new Intent(Registo.this, Login.class));
                        }
                    }, 1000);

                } else {
                    Toast.makeText(Registo.this, "Empty Fields", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean isEmpty() {
        if (TextUtils.isEmpty(email.getText().toString()) ||
                TextUtils.isEmpty(password.getText().toString()) ||
                TextUtils.isEmpty(name.getText().toString()) ||
                TextUtils.isEmpty(apelido.getText().toString())
                ) {
            return true;
        } else {
            return false;
        }
    }
}
